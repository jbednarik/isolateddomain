﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IsolatedDomain
{
    public sealed class Isolated<T> : IDisposable
        where T : MarshalByRefObject
    {
        private readonly AppDomain domain;
        private readonly T marshalledObject;
        private volatile bool disposed;

        private Isolated(AppDomain domain, T marshalledObject)
        {
            this.domain = domain;
            this.marshalledObject = marshalledObject;
        }

        public T Object
        {
            [System.Diagnostics.DebuggerStepThrough]
            get { return marshalledObject; }
        }

        public void Dispose()
        {
            if (disposed) return;
            disposed = true;

            try
            {
                AppDomain.Unload(domain);
            }
            catch { }

            GC.SuppressFinalize(this);
        }

        public static Isolated<T> Create()
        {
            return Create(AppDomain.CurrentDomain.SetupInformation);
        }

        public static Isolated<T> Create(AppDomainSetup domainSetupInfo)
        {
            return Create(domainSetupInfo, null);
        }

        public static Isolated<T> Create(Action<T> onCreated)
        {
            return Create(AppDomain.CurrentDomain.SetupInformation, onCreated);
        }

        public static Isolated<T> Create(AppDomainSetup domainSetupInfo, Action<T> onCreated)
        {
            var domain = AppDomain.CreateDomain("Isolated: " + Guid.NewGuid().ToString(), null, domainSetupInfo);
            Type type = typeof(T);
            object obj = domain.CreateInstanceAndUnwrap(type.Assembly.FullName, type.FullName);
            var marshalledObj = (T)obj;

            if (onCreated != null) onCreated(marshalledObj);

            var isolated = new Isolated<T>(domain, marshalledObj);
            return isolated;
        }
    }
}
