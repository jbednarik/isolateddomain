﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace IsolatedDomain.Tests
{
    [TestClass]
    public class IsolatedTests
    {
        private const string STR_Ahoj = "ahoj";

        [TestMethod]
        public void IsolatedObjectTest()
        {
            using (Isolated<Work> isolated = Isolated<Work>.Create())
            {
                isolated.Object.DoSomething(STR_Ahoj);
                Assert.AreEqual(STR_Ahoj, isolated.Object.value);
            }
        }

        [TestMethod]
        public void IsolatedObjectExceptionTest()
        {

            bool contains = false;
            using (Isolated<WorkCrashTest> isolated = Isolated<WorkCrashTest>.Create())
            {
                // check if the domain loaded
                contains = ListProcessAppDomains.GetAppDomains().Any(x => x.FriendlyName.StartsWith("Isolated"));
                Assert.IsTrue(contains);

                isolated.Object.DoSomething(STR_Ahoj);
            }

            // check if the domain is property unloaded 
            contains = ListProcessAppDomains.GetAppDomains().Any(x => x.FriendlyName.StartsWith("Isolated"));
            Assert.IsFalse(contains);
        }
    }

    public class Work : MarshalByRefObject
    {
        public string value;

        public void DoSomething(string value)
        {
            this.value = value;
        }
    }

    public class WorkCrashTest : MarshalByRefObject
    {
        public string value;

        public void DoSomething(string value)
        {
            throw new Exception("Crash test exception.");
        }
    }
}
